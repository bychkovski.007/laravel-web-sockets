<?php

namespace App\Events;

use App\Http\Resources\Message\MessageResource;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SendLikeEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private int $user_id;
    private string $likeStr;

    /**
     * Create a new event instance.
     */
    public function __construct(int $user_id, string $likeStr)
    {
        //
        $this->user_id = $user_id;
        $this->likeStr = $likeStr;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return array<int, \Illuminate\Broadcasting\Channel>
     */
    public function broadcastOn(): array
    {
        return [
            new PrivateChannel('send_like_'.$this->user_id),
        ];
    }
    public function broadcastAs(): string
    {
        return 'send_like';
    }

    public function broadcastWith() : array {
        return [
            'like_str' => $this->likeStr
        ];
    }
}
