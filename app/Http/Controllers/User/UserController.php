<?php

namespace App\Http\Controllers\User;

use App\Events\SendLikeEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\SendLikeRequest;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function show(User $user){
        return inertia('User/Show', compact('user'));
    }
    public function sendLike(User $user, SendLikeRequest $request){
        $data = $request->validated();
        $likeStr = 'Youre like from user with id' . ' '. $data['from_id'];

        broadcast(new SendLikeEvent($user->id, $likeStr))->toOthers();

        return response()->json([
            'like_str' => $likeStr
        ]);

    }
}
